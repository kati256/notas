/*
 * Descripción: Ejemplo de programa de multiples procesos.
 * Autor: Andrés Villagra de la Fuente.
 *
 * Fecha: 18/08/18
 * */

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>

int is_child(pid_t pid) { return pid == 0; }

int main() {

  printf("Starting first process\n");
  pid_t pid = fork();
  int var = 0;
  int status;

  if (!is_child(pid)) {
    printf("Hello, I am the parent and my child's pid is %d.\n", pid);
    var = 100;
  } else {
    printf("Hello I am the child as my PID is %d.\n", pid);
    var = 200;
  }
  printf("My local variable is %d and it's at 0x%x.\n", var, &var);

  if (is_child(pid)) {
    printf("I'll now call ls.\n");
    const char args[1] = {"."};
    execv("/bin/ls", args);
  }

  pid = wait(&status);
  printf("My child of PID %d finished with status %d.\n", pid, status);

  return 0;

}

